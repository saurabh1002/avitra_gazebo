#include <algorithm>
#include <boost/bind.hpp>
#include <chrono>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <iostream>
#include <thread>

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float32.h>
#include <gazebo_msgs/ModelStates.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Int16.h>
#include <ros/callback_queue.h>
#include <ros/subscribe_options.h>

#define L 0.355 // distance between body center and wheel center
#define r 0.01875 // wheel radius

#define pi30 0.1047197551196597705355242034774843062905347323976457118988037109375 // long double thirty = 30; long double mOne = -1; printf("%1.70Lf\n", (long double) acos(mOne) / thirty);
#define pi 3.141592653589793115997963468544185161590576171875 // long double mOne = -1; printf("%1.70Lf\n", (long double) acos(mOne));
#define pi2 6.28318530717958623199592693708837032318115234375 // long double two = 2; long double mOne = -1; printf("%1.70Lf\n", (long double) two * acos(mOne));
#define Vmax 1.0 // set by experiments (m/s)
#define omegamax 50.0 // set by experiments (rad/s)
#define P 5.0 // PID
#define I 0.0005 // PID
#define stopDistance 0.001
#define stopAngle pi30

namespace gazebo {

    class BasePlugin : public ModelPlugin {


        // Pointer to the model
        private: std::unique_ptr<ros::NodeHandle> rosNode;

        private: ros::Subscriber rosSub;

        private: ros::Subscriber rosSub1;

        private: ros::Publisher rosPub;

        private: ros::CallbackQueue rosQueue;

        private: std::thread rosQueueThread;

        private: physics::WorldPtr world;

        private: physics::ModelPtr model;

        private: physics::ModelPtr LeftWheel;

        private: physics::ModelPtr RightWheel;
        
        private: physics::ModelPtr FrontWheel;
        
        private: physics::ModelPtr BackWheel;

        private: physics::JointPtr RightJoint;

        private: physics::JointPtr BackJoint;

        private: physics::JointPtr LeftJoint;

        private: physics::JointPtr FrontJoint;

        private: physics::JointPtr waist_prismatic;

        private: physics::JointPtr servo_5;

        private: physics::JointPtr servo_6;

        private: physics::JointPtr servo_1_3;

        private: physics::JointPtr servo_2_4;

        public: int update_num = 0;

        public: float scale = 1.0;

        public: float x=0.0, y=0.0;

        // Pointer to the update event connection
        private: event::ConnectionPtr updateConnection;

        private: void QueueThread()
        {
            static const double timeout = 0.01;
            while (this->rosNode->ok())
            {
                this->rosQueue.callAvailable(ros::WallDuration(timeout));
            }
        }

        public: void OdomPub(const gazebo_msgs::ModelStates::ConstPtr &odom)
        {
            ROS_INFO("subscribed to model_states");
            nav_msgs::Odometry odometry;
            odometry.header.frame_id = "odom";
            odometry.child_frame_id = "base_link";
            odometry.pose.pose = odom->pose[1];
            odometry.twist.twist = odom->twist[1];
            rosPub.publish(odometry);
        }

        public: void MoveAvitra(const std_msgs::Int16::ConstPtr &cmd_msg)
        {
            ROS_INFO("Move Avitra - Callback");
            ROS_INFO("%d", cmd_msg->data);
            switch (cmd_msg->data)
            {
                case 1:
                    x = 0.1*scale;
                    y = 0.0;
                    break;

                case 2:
                    x = -0.1*scale;
                    y = 0.0;
                    break;

                case 3:
                    x = 0.0;
                    y = 0.1*scale;
                    break;

                case 4:
                    x = 0.0;
                    y = -0.1*scale;
                    break;

                case 5:
                    scale++;
                    break;

                case 6:
                    scale--;
                    break;

                case 7:
                    ROS_INFO("in brake");
                    x = 0.0;
                    y = 0.0;
                    break;

                default:
                    ROS_INFO("in default");
                    break;
            }
        }

        public: void GetNestedModels()
        {
            this->LeftWheel = model->NestedModel("left_wheel");
            this->RightWheel = model->NestedModel("right_wheel");
            this->FrontWheel = model->NestedModel("front_wheel");
            this->BackWheel = model->NestedModel("back_wheel");
        }

        public: void GetRollerJoints()
        {
            this->LeftWheel->GetJoint("omni_wheel_s_joint")->SetPosition(0, 0);
            this->LeftWheel->GetJoint("omni_wheel_w_joint")->SetPosition(0, 0);
            this->LeftWheel->GetJoint("omni_wheel_n_joint")->SetPosition(0, 0);
            this->LeftWheel->GetJoint("omni_wheel_e_joint")->SetPosition(0, 0);
            this->LeftWheel->GetJoint("omni_wheel_ne_joint")->SetPosition(0, 0);
            this->LeftWheel->GetJoint("omni_wheel_se_joint")->SetPosition(0, 0);
            this->LeftWheel->GetJoint("omni_wheel_sw_joint")->SetPosition(0, 0);
            this->LeftWheel->GetJoint("omni_wheel_nw_joint")->SetPosition(0, 0);
            this->RightWheel->GetJoint("omni_wheel_s_joint")->SetPosition(0, 0);
            this->RightWheel->GetJoint("omni_wheel_w_joint")->SetPosition(0, 0);
            this->RightWheel->GetJoint("omni_wheel_n_joint")->SetPosition(0, 0);
            this->RightWheel->GetJoint("omni_wheel_e_joint")->SetPosition(0, 0);
            this->RightWheel->GetJoint("omni_wheel_ne_joint")->SetPosition(0, 0);
            this->RightWheel->GetJoint("omni_wheel_se_joint")->SetPosition(0, 0);
            this->RightWheel->GetJoint("omni_wheel_sw_joint")->SetPosition(0, 0);
            this->RightWheel->GetJoint("omni_wheel_nw_joint")->SetPosition(0, 0);
            this->FrontWheel->GetJoint("omni_wheel_s_joint")->SetPosition(0, 0);
            this->FrontWheel->GetJoint("omni_wheel_w_joint")->SetPosition(0, 0);
            this->FrontWheel->GetJoint("omni_wheel_n_joint")->SetPosition(0, 0);
            this->FrontWheel->GetJoint("omni_wheel_e_joint")->SetPosition(0, 0);
            this->FrontWheel->GetJoint("omni_wheel_ne_joint")->SetPosition(0, 0);
            this->FrontWheel->GetJoint("omni_wheel_se_joint")->SetPosition(0, 0);
            this->FrontWheel->GetJoint("omni_wheel_sw_joint")->SetPosition(0, 0);
            this->FrontWheel->GetJoint("omni_wheel_nw_joint")->SetPosition(0, 0);
            this->BackWheel->GetJoint("omni_wheel_s_joint")->SetPosition(0, 0);
            this->BackWheel->GetJoint("omni_wheel_w_joint")->SetPosition(0, 0);
            this->BackWheel->GetJoint("omni_wheel_n_joint")->SetPosition(0, 0);
            this->BackWheel->GetJoint("omni_wheel_e_joint")->SetPosition(0, 0);
            this->BackWheel->GetJoint("omni_wheel_ne_joint")->SetPosition(0, 0);
            this->BackWheel->GetJoint("omni_wheel_se_joint")->SetPosition(0, 0);
            this->BackWheel->GetJoint("omni_wheel_sw_joint")->SetPosition(0, 0);
            this->BackWheel->GetJoint("omni_wheel_nw_joint")->SetPosition(0, 0);
        }

        public: void GetJointsFromModel(physics::ModelPtr _model)
        {
            unsigned int c = this->model->GetJointCount();
            ROS_INFO("There are %d joints in the model", c);
            this->waist_prismatic = _model->GetJoint("waist_prismatic");
            this->servo_2_4 = _model->GetJoint("servo_2_4");
            this->servo_1_3 = _model->GetJoint("servo_1_3");
            this->servo_5 = _model->GetJoint("servo_5");
            this->servo_6 = _model->GetJoint("servo_6");
            this->LeftJoint = _model->GetJoint("left_wheel");          
            this->RightJoint = _model->GetJoint("right_wheel");          
            this->FrontJoint = _model->GetJoint("front_wheel");          
            this->BackJoint = _model->GetJoint("back_wheel");          
        }

        public: void ros_init()
        {
            ROS_INFO("init ros");
            if (!ros::isInitialized())
            {
                ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load plugin. "
                << "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
                return;
            }
            this->rosNode.reset(new ros::NodeHandle("avitra"));   
        }

        public: void create_pubsub()
        {
            ROS_INFO("created subscriber");
            ros::SubscribeOptions so = ros::SubscribeOptions::create<std_msgs::Int16>("command", 1, boost::bind(&BasePlugin::MoveAvitra, this, _1), ros::VoidPtr(), &this->rosQueue);
            this->rosSub = this->rosNode->subscribe(so);
            ros::SubscribeOptions so1 = ros::SubscribeOptions::create<gazebo_msgs::ModelStates>("/gazebo/model_states", 1, boost::bind(&BasePlugin::OdomPub, this, _1), ros::VoidPtr(), &this->rosQueue);
            this->rosSub1 = this->rosNode->subscribe(so1);  

            this->rosPub = this->rosNode->advertise<nav_msgs::Odometry>("odom", 1);         
        }

        public: void move_avitra(float x, float y)
        {
            this->model->SetLinearVel(math::Vector3(x,y,0));
            this->model->SetAngularVel(math::Vector3(0,0,0));
        }


        public: void Load(physics::ModelPtr _model, sdf::ElementPtr /*_sdf*/) 
        {
            ROS_INFO("The model is loaded\nExtracting Joints.....");

            this->model = _model;
            this->world = _model->GetWorld();

            GetNestedModels();

            GetJointsFromModel(this->model);

            // this->model->GetJointController()->SetVelocityPID(this->LeftJoint->GetScopedName(), common::PID(0.1,0.01,0.001));
            // this->model->GetJointController()->SetVelocityPID(this->RightJoint->GetScopedName(), common::PID(0.1,0.01,0.001));

            // this->model->GetJointController()->SetVelocityPID(this->servo_2_4->GetScopedName(), common::PID(10,0,0));
            // this->model->GetJointController()->SetVelocityPID(this->servo_1_3->GetScopedName(), common::PID(10,0,0));
            // this->model->GetJointController()->SetPositionPID(this->servo_5->GetScopedName(), common::PID(0.1,0,0));
            // this->model->GetJointController()->SetPositionPID(this->servo_6->GetScopedName(), common::PID(0.1,0,0));

            ros_init();

            create_pubsub();

            this->rosQueueThread = std::thread(std::bind(&BasePlugin::QueueThread, this));
            // Listen to the update event. This event is broadcast every simulation iteration.
            this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&BasePlugin::OnUpdate, this, _1));
        }

        public: void OnUpdate(const common::UpdateInfo & /*_info*/) 
        {
            ROS_INFO("update");
                    
            // this->model->GetJointController()->SetVelocityTarget(this->LeftJoint->GetScopedName(), 0.0);
            // this->model->GetJointController()->SetVelocityTarget(this->RightJoint->GetScopedName(), 0.0);
            // this->model->SetAngularVel(math::Vector3(0,0,0));
            // this->model->SetLinearVel(math::Vector3(0,0,0));
            this->servo_5->SetPosition(0, 0);
            this->servo_6->SetPosition(0, 0);
            this->servo_2_4->SetPosition(0, 0);
            this->servo_1_3->SetPosition(0, 0);
            this->waist_prismatic->SetPosition(0, 0);
            // this->LeftJoint->SetPosition(0, 0);
            // this->RightJoint->SetPosition(0, 0);
            // this->FrontJoint->SetPosition(0, 0);
            // this->BackJoint->SetPosition(0, 0);

            GetRollerJoints();

            move_avitra(x, y);

            }

    };

    GZ_REGISTER_MODEL_PLUGIN(BasePlugin)
}
